class Book < ApplicationRecord
  belongs_to :author
  belongs_to :publisher
  has_and_belongs_to_many :readers


   validates :name, presence: true, length: {minimum:2, maximum: 30}
   validates :summary, presence: true, length: {minimum:10, maximum: 250}
   validates :isbn_no, presence: true, numericality: {greater_than: 0}
   validates :page_count, presence: true, numericality: {greater_than: 0}

  def as_json(options = {})
    super(options.merge(include: [:author, :publisher, :readers]))
  end
end
