class Publisher < ApplicationRecord
    has_many :books

    validates :name, presence: true, length: {minimum:2, maximum: 30}
    validates :address, confirmation: true
  
end
