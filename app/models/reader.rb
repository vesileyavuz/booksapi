class Reader < ApplicationRecord
    has_and_belongs_to_many :books

  
    validates :name, presence: true, length: {minimum:2, maximum: 30}
    validates :surname, presence: true, length: {minimum:2, maximum: 30}
    validates :email, presence: true, format: { with: URI::MailTo::EMAIL_REGEXP, message: "is not a valid email address" }
  
    def as_json(options = {})
    super(options.merge(include: [:books]))
  end
end