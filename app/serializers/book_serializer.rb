class BookSerializer < ActiveModel::Serializer
    attributes :id, :name, :summary, :isbn_no, :page_count, :printing_date, :created_at, :updated_at
  
    belongs_to :author
    belongs_to :publisher
    
    
 
  end
  