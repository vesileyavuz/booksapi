class Api::V1::ReadersController < ApplicationController
    before_action :set_reader, only: [:show, :update, :destroy]

    def index
        @readers = Reader.all
        render json: @readers
    end

    def show
        render json: @reader.as_json(only: [:id, :name, :surname, :email]), status: :ok
    end

    def create
        @reader = Reader.create(reader_params)
        if  @reader.save
           
        render json: @reader, status: :created
        else
            render json: { errors: @reader.errors.full_messages }, status: :unprocessable_entity    
        end
    end

    def update
        if @reader.update(reader_params)
            render json: @reader
        else
            render json: @reader.errors, status: :unprocessable_entity
          end    
        end

    def destroy
        unless @reader&.destroy
            render json: { errors: 'Okuyucu Bulunamadı.'}, status: :not_found 
        else
            render json: { notice: 'Okuyucu başarıyla silindi'}
        end 
           
    end

    private
  
    def set_reader
      @reader = Reader.find(params[:id])
    end
    
  
    def reader_params
        params.permit(:name, :surname, :email)
      end
end
