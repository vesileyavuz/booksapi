module Api::V1 
  class BooksController < ApplicationController
      before_action :set_book, only: [:show, :update, :destroy]
      
  def index
    @books = Book.all
    render json: @books, include: [:author, :publisher, :reader], status: :ok
  end

  def show
    @book = Book.includes(:author, :publisher, :readers).find(params[:id])
    render json: @book.as_json(include: [:author, :publisher, :readers])
  end
 

  def create
    @book = Book.new(book_params)
    if @book.save
      
      render json: @book, include: [:author, :publisher, :reader], status: :created
    else
      render json: { errors: @book.errors.full_messages }, status: :unprocessable_entity
    end
  end
  
      
  def update
    if @book.update(book_params)
      render json: @book, include: [:author, :publisher, :reader]
    else
      render json: @book.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @book = Book.find_by_id(params[:id])
    unless @book&.destroy
      render json: { errors: 'Kitap bulunamadı' }, status: :not_found
        return
    end
      
      render json: { notice: 'Kitap başarıyla silindi' }
  end
      

  private

  def set_book
    @book = Book.includes(:author, :publisher).find(params[:id])
  end
  

    def book_params
      params.require(:book).permit(:name, :summary, :isbn_no, :page_count, :printing_date, :publisher_id, :author_id, :reader_id, publisher: [:name, :address], author: [:name, :surname], reader: [:name, :surname, :email])
    end
   
    
  
  end
end