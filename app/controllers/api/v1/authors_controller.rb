module Api::V1 
class AuthorsController < ApplicationController
    before_action :set_author, only: [:show, :update, :destroy]

  
  def index
    @authors = Author.all

    render json: @authors
  end

 
  def show
    render json: @author
  end

  
  def create
    @author = Author.create(author_params)
    if @author.save
      
      render json: @author, status: :created
    else
      render json:{ errors: @author.errors.full_messages }, status: :unprocessable_entity
    end
  end

  
  def update
    if @author.update(author_params)
      render json: @author
    else
      render json: @author.errors, status: :unprocessable_entity
    end
  end

 
  def destroy
    @author = Author.find_by_id(params[:id])
    unless @author&.destroy
      render json: { errors: 'Yazar Bulunamadı.'}, status: :not_found
    end
      render json: { notice: 'Yazar başarıyla silindi'}
  end

  private
    
    def set_author
      @author = Author.find(params[:id])
    end

   
    def author_params
      params.permit(:name, :surname)
    end
end
end
