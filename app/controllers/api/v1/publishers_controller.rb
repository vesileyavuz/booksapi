module Api::V1 
class PublishersController < ApplicationController
    before_action :set_publisher, only: [:show, :update, :destroy]
  
    def index
      @publishers = Publisher.all
      render json: @publishers
    end

  
    def show
      render json: @publisher
    end

  
    def create
      @publisher = Publisher.create(publisher_params)
      if  @publisher.save
       
          render json: @publisher, status: :created
      else
        render json: { errors: @publisher.errors.full_messages }, status: :unprocessable_entity
      end
    end

  
    def update
      if @publisher.update(publisher_params)
        render json: @publisher
      else
        render json: @publisher.errors, status: :unprocessable_entity
      end
    end

  
    def destroy
      unless @publisher&.destroy
        render json: { errors: 'Yayınevi Bulunamadı.'}, status: :not_found
      else
        render json: { notice: 'Yayınevi başarıyla silindi'}
      end
    end
  
    private
  
    def set_publisher
      @publisher = Publisher.find(params[:id])
    end
    
  
    def publisher_params
      params.permit(:name, :address)
    end
  end
end 
  