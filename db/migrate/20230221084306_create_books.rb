class CreateBooks < ActiveRecord::Migration[7.0]
  def change
    create_table :authors do |t|
      t.string :name
      t.string :surname
      t.timestamps
    end


    create_table :publishers do |t|
      t.string :name
      t.string :address
      t.timestamps
    end
    

    create_table :books do |t|
      t.references :publisher
      t.references :author
      t.references :reader
      t.string :name
      t.text :summary
      t.bigint :isbn_no
      t.integer :page_count
      t.datetime :printing_date
      t.timestamps
    end
  end
end

class AddAuthorNameAndAuthorSurnameToBooks < ActiveRecord::Migration[7.0]
  def change
    add_column :books, :author_name, :string
    add_column :books, :author_surname, :string
  end
end




