class AddReaderToBooks < ActiveRecord::Migration[6.1]
  def change
    unless column_exists?(:books, :reader_id)
      add_reference :books, :reader, foreign_key: true, null: true
    end
  end
  
end
