class CreateJoinTableReadersBooks < ActiveRecord::Migration[7.0]
  def change
    create_join_table :readers, :books do |t|
       t.index [:reader_id, :book_id]
       t.index [:book_id, :reader_id]
    end
  end
end
