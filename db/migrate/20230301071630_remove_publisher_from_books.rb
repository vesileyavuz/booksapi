class RemovePublisherFromBooks < ActiveRecord::Migration[7.0]
  def change
    if column_exists? :books, :publisher
      remove_column :books, :publisher
    end
  end
end
