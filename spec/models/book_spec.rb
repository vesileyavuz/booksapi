require 'rails_helper'

RSpec.describe Book, type: :model do
  let(:author) { FactoryBot.create(:author) }
  let(:publisher) { FactoryBot.create(:publisher) }

  describe "validations" do
    it { should validate_presence_of(:name) }
    it { should validate_length_of(:name).is_at_least(2) }
    it { should validate_presence_of(:summary) }
    it { should validate_length_of(:summary).is_at_least(10) }
    it { should validate_presence_of(:isbn_no) }
    it { should validate_presence_of(:page_count) }
  end

  describe "associations" do
    it { should belong_to(:author) }
    it { should belong_to(:publisher) }
  end
end
