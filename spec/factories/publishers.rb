require 'faker'
FactoryBot.define do
  factory :publisher do
    name { Faker::Book.publisher }
    address { Faker::Address.full_address }
  end
end