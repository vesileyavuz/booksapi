require 'faker'
FactoryBot.define do
  factory :reader do
    name { Faker::Name.name }
    surname { Faker::Name.last_name }
    email { Faker::Internet.email }
   

    trait :with_books do
      books { build_list(:book, 3, :with_reader, reader: nil) }
    end
    
  end
end
