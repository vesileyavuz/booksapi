require 'faker'

FactoryBot.define do
  factory :book do
    name { Faker::Book.title }
    summary { Faker::Lorem.paragraph }
    isbn_no { Faker::Number.number(digits: 10) }
    page_count { Faker::Number.number(digits: 3) }

    association :author, factory: :author
    association :publisher, factory: :publisher

    trait :with_reader do
      after(:create) do |book|
        create(:reader_book, book: book)
      end
    end

    trait :with_readers do
      after(:create) do |book|
        create_list(:reader_book, 3, book: book)
      end
    end
    
    end
  end
