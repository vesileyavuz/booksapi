require 'rails_helper'
require 'faker'

RSpec.describe 'Books API', type: :request do

  describe 'GET /books' do
    before do
      @author = FactoryBot.create(:author)
      @publisher = FactoryBot.create(:publisher)
      @reader = FactoryBot.create(:reader)
      @book = FactoryBot.create(:book, :with_readers, author: @author, publisher: @publisher, readers: [@reader])

      5.times do
        FactoryBot.create(:book, :with_readers, author: @author, publisher: @publisher, isbn_no: Faker::Number.number(digits: 10), page_count: Faker::Number.number(digits: 3), readers: [reader])
      end
    end
    
    it 'returns all books' do
      get "/api/v1/books"
      expect(response).to have_http_status(:success)
      expect(JSON.parse(response.body).size).to eq(6)
    end
  end
end
