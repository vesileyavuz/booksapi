Rails.application.routes.draw do
  root 'welcome#index'
  namespace :api do
    namespace :v1 do
      resources :publishers
      resources :books
      resources :authors
      resources :readers
    end
  end
end


